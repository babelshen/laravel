<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('page.title', config('app.name'))</title>
    <script src="https://cdn.tailwindcss.com"></script>
</head>
<body class='flex flex-col justify-between min-h-screen'>
    @include('layouts.header')
    <main class='flex-1	my-7 w-9/12	mx-auto'>
        @yield('content')
    </main>
    @include('layouts.footer')
</body>
</html>