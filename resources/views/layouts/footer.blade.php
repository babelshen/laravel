<footer class='bg-gray-800'>
    <x-footer.footer-wrapper>
    {{ config('app.name') . ' ' . date('Y') }}
    </x-footer.footer-wrapper>
</footer>