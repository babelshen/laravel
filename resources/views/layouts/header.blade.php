<header>
  <x-header.header-navigation>
    <x-header.logo />
    <div class="hidden sm:ml-6 sm:block">
      <div class="flex space-x-4">
        <x-header.navigation-link href="{{ route('blogs.index') }}">Blogs</x-header.navigation-link>
        <x-header.navigation-link href="{{ route('auth.sign-in.index') }}">Sign In</x-header.navigation-link>
        <x-header.navigation-link href="{{ route('sign-up.index') }}">Sign Up</x-header.navigation-link>
      </div>
    </div>
  </x-header.header-navigation>
</header>