<div class='py-4 text-white text-center max-w-7xl mx-auto'>
    {{ $slot }}
</div>