@props([
    'type' => 'text',
    'name' => 'email',
    'title' => 'Text',
    'placeholder' => 'Placeholder',
    'value' => '',
])

<div class='flex flex-col gap-1'>
    <label for="{{ $name }}" class="block text-sm font-medium leading-6 text-gray-900"> {{ $title }} </label>
    <div class="relative rounded-md shadow-sm">
        <input 
            type="{{ $type }}"
            name="{{ $name }}"
            id="{{ $name }}"
            class="block w-full rounded-md border-0 py-1.5 pl-4 pr-20 text-gray-900 ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6" 
            placeholder="{{ $placeholder }}"
            value="{{ $value }}"
        />
    </div>
    <x-form.form-error name="{{ $name }}" />
</div>