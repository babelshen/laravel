@props([
    'name' => ''
])

@error($name)
    <div class='text-rose-600'>
        {{ $message }}
    </div>
@enderror