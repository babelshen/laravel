<div class='flex justify-between'>

    @isset($link)

        {{ $link }}

    @endisset

    <h1 class='font-bold text-lg text-center'>
        {{ $slot }}    
    </h1>

    @isset($button)

        {{ $button }}
    
    @endisset
</div>