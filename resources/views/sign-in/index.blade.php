@extends('layouts.base')

@section('page.title', 'Login Page')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('auth.sign-in.store') }}" method="POST">
            <x-form.form-header>Sign in</x-form.form-header>
            <x-form.form-input 
                type="email" 
                name="email" 
                placeholder="Type your e-mail" 
                title="E-mail" 
                value="{{ old('email') }}"
            />
    
            <x-form.form-input 
                type="password" 
                name="password" 
                placeholder="Type your password" 
                title="Password" 
                value="{{ old('password') }}"
            />
            <x-form.form-button>Sign In</x-form.form-button>
        </x-form.form>
    </section>

@endsection