@extends('layouts.base')

@section('page.title', $blog -> title)

@section('content')
    <section>
        <x-pages.page-title>
            {{ $blog -> title }}
        
            <x-slot name='link'>
                <a href="{{ route('blogs.index') }}" >
                    ← Go back
                </a>
            </x-slot>

            <x-slot name='button'>
                <a href="{{ route('blogs.edit', $blog -> id) }}" >
                    Update
                </a>
            </x-slot>
        </x-pages.page-title>

        <div class='m-6'>
            {!! $blog -> content !!}
        </div>
    </section>
@endsection