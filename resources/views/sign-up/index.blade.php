@extends('layouts.base')

@section('page.title', 'Registration Page')

@section('content')

    <section class='max-w-lg mx-auto'>
        <x-form.form action="{{ route('sign-up.store') }}" method="POST">
            <x-form.form-header>Sign up</x-form.form-header>
            <x-form.form-input 
                type="email" 
                name="email" 
                placeholder="Type your e-mail" 
                title="E-mail" 
                value="{{ old('email') }}" 
            />
            <x-form.form-input 
                type="password" 
                name="password" 
                placeholder="Type your password" 
                title="Password" 
                value="{{ request() -> old('password') }}" 
            />
            <x-form.form-input 
                type="password" 
                name="password_confirmation" 
                placeholder="Confirm your password" 
                title="Confirm password" 
                value="{{ old('password_confirmation') }}" 
            />
            <x-form.form-button>Sign up</x-form.form-button>
        </x-form.form>
    </section>

@endsection