<?php

namespace App\Models;

class Blog
{
    protected static $blogs;

    public $id;
    public $title;
    public $content;

    public function __construct($id, $title, $content)
    {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
    }

    protected static function initializeBlogs()
    {
        self::$blogs = collect([
            new Blog(
                1, 
                'sunt aut facere repellat provident occaecati excepturi optio reprehenderit', 
                'quia et <u>suscipit\nsuscipit</u> recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto',
            ),
            new Blog(
                2, 
                'qui est esse', 
                'est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla',
            ),
            new Blog(
                3, 
                'ea molestias quasi exercitationem repellat qui ipsa sit aut', 
                'et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut',
            ),
        ]);
    }

    public static function getAllBlogs($search = null)
    {
        if (is_null(self::$blogs)) {
            self::initializeBlogs();
        }

        if (is_null($search)) {
            return self::$blogs;
        }

        return self::$blogs->filter(function ($blog) use ($search) {
            return strpos($blog->title, $search) !== false;
        });
    }

    public static function getBlogById($id)
    {
        return self::getAllBlogs()->firstWhere('id', $id);
    }

    public static function addBlog($title, $content)
    {
        if (is_null(self::$blogs)) {
            self::initializeBlogs();
        }

        $lastId = self::$blogs->last()->id;

        $newBlog = new Blog($lastId + 1, $title, $content);

        self::$blogs->push($newBlog);

        return $newBlog;
    }

    public static function updateBlog($blogId, $title, $content) 
    {
        $blog = Blog::getBlogById($blogId);

        if (!$blog) {
            return abort(404);
        }
        
        if ($blog->title !== $title) {
            $blog->title = $title;
        }

        if ($blog->content !== $content) {
            $blog->content = $content;
        }
    }

    public static function deleteBlog($blogId)
    {
        $blog = Blog::getBlogById($blogId);

        if ($blog) self::$blogs->forget($blog->id - 1);
    }
}
