<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index(Request $request)
    {
        $search = $request -> input('search');
        $blogs = Blog::getAllBlogs($search);
        return view('blogs.index', compact('blogs'));
    }

    public function show($blogId)
    {
        $blog = Blog::getBlogById($blogId);
        if (!$blog) {
            abort(404);
        }
        return view('blogs.show', compact('blog'));
    }

    public function create() 
    {
        return view('blogs.create');
    }

    public function store(Request $request)
    {

        $validated = $request -> validate([
            'title' => ['required', 'string', 'min: 5', 'max: 100'],
            'content' => ['required', 'string', 'min: 10', 'max:500'],
        ]);

        if (!$validated) {
            return redirect() -> back() -> withInput();
        }

        $title = $request->input('title');
        $content = $request->input('content');

        Blog::addBlog($title, $content);

        $blogs = Blog::getAllBlogs();

        return view('blogs.index', compact('blogs'));
    }

    public function edit($blogId) 
    {
        $blog = Blog::getBlogById($blogId);
        if (!$blog) {
            abort(404);
        }
        return view('blogs.edit', compact('blog'));
    }

    public function update(Request $request, $blogId) 
    {

        $validated = $request -> validate([
            'title' => ['nullable', 'string', 'min: 5', 'max: 100'],
            'content' => ['nullable', 'string', 'min: 10', 'max:500'],
        ]);

        if (!$validated) {
            return redirect() -> back() -> withInput();
        }

        $title = $request->input('title');
        $content = $request->input('content');

        Blog::updateBlog($blogId, $title, $content);

        $blogs = Blog::getAllBlogs();

        return view('blogs.index', compact('blogs'));
    }

    public function destroy($blogId)
    {
        Blog::deleteBlog($blogId);

        $blogs = Blog::getAllBlogs();

        return view('blogs.index', compact('blogs'));
    }
}
