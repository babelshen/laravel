<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    public function index() 
    {
        return View::make('sign-in.index'); 
    }

    public function store(Request $request) 
    {
        $validated = $request -> validate([
            'email' => ['required', 'string', 'email'],
            'password' => ['required'],
        ]);

        if (!$validated) {
            return redirect() -> back() -> withInput();
        }

        return redirect() -> route('blogs.index');
    }
}
