<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegistrationController;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;

Route::view('/', 'home.index') -> name('home.index');
Route::redirect('/main', '/', 301);

Route::prefix('/blogs') -> group(function()
{   
    Route::get('/', [BlogController::class, 'index']) -> name('blogs.index');
    Route::get('/{blog}', [BlogController::class, 'show']) -> name('blogs.show') -> whereNumber('blog');
    Route::get('/create', [BlogController::class, 'create']) -> name('blogs.create');
    Route::post('/', [BlogController::class, 'store']) -> name('blogs.store');
    Route::get('/{blog}/edit', [BlogController::class, 'edit']) -> name('blogs.edit') -> whereNumber('blog');
    Route::patch('/{blog}', [BlogController::class, 'update']) -> name('blogs.update') -> whereNumber('blog');
    Route::delete('/{blog}', [BlogController::class, 'destroy'])->name('blogs.destroy') -> whereNumber('blog');
});

Route::resource('/sign-up', RegistrationController::class)->only([
    'index', 'store',
]) -> middleware('guest');

Route::prefix('/auth')->as('auth.')->group(function()
{   
    Route::redirect('/', '/auth/sign-in', 301);
    Route::get('/sign-in', [LoginController::class, 'index'])->name('sign-in.index')->middleware('guest'); 
    Route::post('/sign-in', [LoginController::class, 'store'])->name('sign-in.store')->middleware('guest'); 
});

Route::fallback(function() 
    {
        return Redirect::to('/');
    }
);